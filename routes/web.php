<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('about-us', function () {
    return view('about-us');
});

Route::get('next', function () {
    return view('enter');
});

Route::get('events', function () {
    return view('events');
});

Route::get('tariff', function () {
    return view('tariff');
});

Route::get('gallery', function () {
    return view('gallery');
});

Route::get('contact', function () {
    return view('contact');
});

Auth::routes();

Route::group(['middleware' => ['roles']], function () {

	Route::get('/home', 'HomeController@index')->name('home');
   	Route::get('admin', 'Admin\AdminController@index');
	Route::resource('admin/roles', 'Admin\RolesController');
	Route::resource('admin/permissions', 'Admin\PermissionsController');
	Route::resource('admin/users', 'Admin\UsersController');
	Route::resource('admin/pages', 'Admin\PagesController');
	Route::resource('admin/activitylogs', 'Admin\ActivityLogsController')->only([
	    'index', 'show', 'destroy'
	]);
	Route::resource('admin/settings', 'Admin\SettingsController');
	Route::get('admin/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@getGenerator']);
	Route::post('admin/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@postGenerator']);

	Route::resource('bookings', 'Bookings\\BookingsController');

});

