<div class="form-group {{ $errors->has('booking_id') ? 'has-error' : ''}}">
    {!! Form::label('booking_id', 'Booking Id', ['class' => 'control-label']) !!}
    {!! Form::text('booking_id', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('booking_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('from_date') ? 'has-error' : ''}}">
    {!! Form::label('from_date', 'From Date', ['class' => 'control-label']) !!}
    {!! Form::date('from_date', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('from_date', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('to_date') ? 'has-error' : ''}}">
    {!! Form::label('to_date', 'To Date', ['class' => 'control-label']) !!}
    {!! Form::date('to_date', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('to_date', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('package') ? 'has-error' : ''}}">
    {!! Form::label('package', 'Package', ['class' => 'control-label']) !!}
    {!! Form::text('package', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('package', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('timing') ? 'has-error' : ''}}">
    {!! Form::label('timing', 'Timing', ['class' => 'control-label']) !!}
    {!! Form::input('time', 'timing', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('timing', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('function_id') ? 'has-error' : ''}}">
    {!! Form::label('function_id', 'Function Id', ['class' => 'control-label']) !!}
    {!! Form::number('function_id', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('function_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('function_id_value') ? 'has-error' : ''}}">
    {!! Form::label('function_id_value', 'Function Id Value', ['class' => 'control-label']) !!}
    {!! Form::number('function_id_value', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('function_id_value', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('number_of_people') ? 'has-error' : ''}}">
    {!! Form::label('number_of_people', 'Number Of People', ['class' => 'control-label']) !!}
    {!! Form::text('number_of_people', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('number_of_people', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('catering') ? 'has-error' : ''}}">
    {!! Form::label('catering', 'Catering', ['class' => 'control-label']) !!}
    {!! Form::number('catering', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('catering', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('photography') ? 'has-error' : ''}}">
    {!! Form::label('photography', 'Photography', ['class' => 'control-label']) !!}
    {!! Form::number('photography', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('photography', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('video') ? 'has-error' : ''}}">
    {!! Form::label('video', 'Video', ['class' => 'control-label']) !!}
    {!! Form::number('video', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('video', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('event_management') ? 'has-error' : ''}}">
    {!! Form::label('event_management', 'Event Management', ['class' => 'control-label']) !!}
    {!! Form::number('event_management', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('event_management', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('travel_arrangement') ? 'has-error' : ''}}">
    {!! Form::label('travel_arrangement', 'Travel Arrangement', ['class' => 'control-label']) !!}
    {!! Form::number('travel_arrangement', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('travel_arrangement', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('rooms') ? 'has-error' : ''}}">
    {!! Form::label('rooms', 'Rooms', ['class' => 'control-label']) !!}
    {!! Form::number('rooms', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('rooms', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('g_name') ? 'has-error' : ''}}">
    {!! Form::label('g_name', 'G Name', ['class' => 'control-label']) !!}
    {!! Form::text('g_name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('g_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('g_email') ? 'has-error' : ''}}">
    {!! Form::label('g_email', 'G Email', ['class' => 'control-label']) !!}
    {!! Form::text('g_email', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('g_email', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('g_phone') ? 'has-error' : ''}}">
    {!! Form::label('g_phone', 'G Phone', ['class' => 'control-label']) !!}
    {!! Form::text('g_phone', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('g_phone', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('g_address') ? 'has-error' : ''}}">
    {!! Form::label('g_address', 'G Address', ['class' => 'control-label']) !!}
    {!! Form::textarea('g_address', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('g_address', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('g_city') ? 'has-error' : ''}}">
    {!! Form::label('g_city', 'G City', ['class' => 'control-label']) !!}
    {!! Form::text('g_city', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('g_city', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('g_state') ? 'has-error' : ''}}">
    {!! Form::label('g_state', 'G State', ['class' => 'control-label']) !!}
    {!! Form::text('g_state', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('g_state', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('g_pincode') ? 'has-error' : ''}}">
    {!! Form::label('g_pincode', 'G Pincode', ['class' => 'control-label']) !!}
    {!! Form::text('g_pincode', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('g_pincode', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
