@include('front_header')
    
    <section class="section">
      <div class="container">
        <!-- ROW -->
        <div class="row">
          <!-- COLUMN -->
          <div class="col-md-3">
            <div class="banner-left hide-xs">
             <img src="{{'front_assets/images/banner/3.jpg'}}" class="img-fluid " alt="Mini Hall in Coimbatore"/>
             <div class="overflow">
               <span>Suite & guest rooms</br> with AC</span>
               <a href="tariff.html#exampleModal">Book Now</a>
             </div>           </div>
          </div><!-- /. COLUMN -->
           <!-- COLUMN -->
          <div class="col-md-6">
            <div class="grid">
              <h3>With The Blessings of Maha Periyava</h3>
              <p>Jagadguru Shri Chandrasekharendra Saraswati Swamigal (20 May 1894 – 8 January 1994), reverently known as the Mahaperiyava  or the Sage of Kanchi was the 68th Jagadguru of the Kanchi Kamakoti Peetham. The beacon of eternal light in our life exists with the Blessings from this Great Saint and Acharya of the last century who co-existed amongst us to preach the Hindu philosophy and Santana Dharma. Maha Periayava has always been the paragon of beatitude and epitome of universal love. </p>
              <p>His prolific discourses and profound wisdom helped people to subscribe themselves to a simple, pure, dharmic life. Maha Periayava prescribed discipline and diligence in performing one’s duty to observe Dharma and to be socially responsible. As promoters of this Hall, we have been significant benefactors of his preaching & blessings and we consider it our duty to propagate his teaching.</p>
              <p>We have many festivals and functions that we observe in our life, rituals of which have a specific meaning and a certain methodology to be followed. We consider that, dedicating this hall for the service and guidance of this generation and helping them perform these rituals in the path shown by the holy saint, will be an apt way of showing our gratitude to his holiness, The Maha Periyava</p>
              <p>We are truly blessed to follow Maha Periyava’s spiritual teachings and guidelines. May his devotees of this generation and the generations to come be blessed in this hall while performing their rituals in the path tread by the holy saint.</p>
              <h5 class="font">HARA HARA SANKARA JAYA JAYA SANKARA!!</h5>

             
            </div>
          </div><!-- /. COLUMN -->
           <!-- COLUMN -->
          <div class="col-md-3">
            <div class="banner-right hide-xs">
             <img src="{{'front_assets/images/banner/1.jpg'}}" class="img-fluid " alt="Mini Hall in Coimbatore"/>
             <div class="overflow">
              <h5><span>with blessings of</span></br> maha Periyava</h5>
               <p>make every event in your life most memorable and filled with divinity </p>
             </div>
           </div>
          </div><!-- /. COLUMN -->
        </div><!-- /.ROW -->
      </div>
    </section>
     
@include('front_footer')




