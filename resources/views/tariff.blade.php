@include('front_header')
    
      
    <section class="section tariff">
      <div class="container">
        <!-- ROW -->
        <div class="row">
         
           <!-- COLUMN -->
          <div class="col-md-12">
            <div class="grid">
              <h3>Tariff</h3>
              <p>We value your time and money by providing a collaborative package of facilities. </p>
              
            

             <h4 class="sub-content">The tariff and plans are open to your preferences (GST APPLICABLE):</h4>
             <!-- <img src="images/tariff.png" class="img-fluid" alt=""/> -->

             <div class="row equal-height">
              <div class="col-md-6 col-sm-6 align-self-center align-center">
                <img src="images/package.png" class="img-fluid" alt=""/>
              </div>
              <div class="col-md-6 col-sm-6 align-self-center">
                <div class="btn-grid">
                   <button type="button" class="btn" data-toggle="modal" data-target="#exampleModal">
                    Enquire Now
                  </button>
                 </div>
              </div>
            </div>
             
             <!-- <ul>
              <li class="li1">
                <h2>Plan For</h2>
                <h4>12 hours</h4>
                <p>Rent Inclusive of 4 Rooms</p>
              </li>
               <li class="li2">
                <h2>Plan For</h2>
                <h4>24 hours</h4>
                <p>Rent Inclusive of 4 Rooms</p>
              </li>
               <li class="li3">
                <h2>Plan For</h2>
                <h4>36 hours</h4>
                <p>Rent Inclusive of 4 Rooms</p>
              </li>
               <li class="li4">
                <h2>Plan For</h2>
                <h4>48 hours</h4>
                <p>Rent Inclusive of 4 Rooms</p>
              </li>
            </ul> -->
             

            <p>On the other hand, you could also occupy our rooms without availing the catering resource. Your needs can be customized as we do not compartmentalize the facilities and rates. Right from room allocation to catering facility or videography, every facility is optional and made affordable. </p>

            <h4 class="sub-content">Rooms Tariff:</h4>
            <table class="table table-striped">
            <tbody>
              <tr>
                <td>Suite Rooms Rent (INR.) For 24 Hours - Net Rent</td>
                <td class="align-right">3,500.00</td>
              </tr>
              <tr>
               
                <td>Only Second Floor Room Rent (INR.) For 24 Hours - Net Rent</td>
                <td class="align-right">2,000.00</td>
              
              </tr>
              
            </tbody>
          </table>

          <p class="align-right red">*(EB and Generator charges Extra)</p>
          <p class="align-right red">*(Genset charges: INR 1,000/hr during power shut down) </p>

          <h4 class="sub-content">Bank Details:</h4>
            <table class="table custom-table">
            <tbody>
              <tr>
                <td>NAME</td>
                <td class="align-center">MAHASANKARA MINI HALL</td>
              </tr>
              <tr>
                <td>ACCOUNT NUMBER </td>
                <td class="align-center">510909010108817</td>
              </tr>
              <tr>
                <td>BANK</td>
                <td class="align-center">CITY UNION BANK</td>
              </tr>
              <tr>
                <td>BRANCH</td>
                <td class="align-center">VADAVALLI</td>
              </tr>
              <tr>
                <td>IFSC</td>
                <td class="align-center">CIUB0000274</td>
              </tr>             
            </tbody>
          </table>



             

            </div>
          </div><!-- /. COLUMN -->
           
        </div><!-- /.ROW -->
      </div>
    </section>
    
@include('front_footer')




