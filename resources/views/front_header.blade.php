<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <title>Mahasankara Mini Hall - Mini Function Hall in Vadavalli, Hall at Vadavalli</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="Mahasankara is one of the best and affordable AC mini halls in Coimbatore, Book our mini hall for all your auspicious celebrations to experience divine bliss."/>
    <!-- Bootstrap CSS -->
    <link href="{{'front_assets/css/bootstrap.min.css'}}" rel="stylesheet" type="text/css" />
    <!-- Style CSS -->
    <link href="{{'front_assets/css/style.css'}}" rel="stylesheet" type="text/css" />
     <!-- Responsive CSS -->
    <link href="{{'front_assets/css/responsive.css'}}" rel="stylesheet" type="text/css" />
     <link rel="shortcut icon" type="image/png" href="{{'front_assets/images/fav.png'}}"/>
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-140595840-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-140595840-1');
</script>

  </head>
  <body>
  

  
 <section class="section no-padding">
   <div class="container">
      <div class="row">
        <div class="col align-self-center align-center">
         <a href=""><img src="{{'front_assets/images/logo_new.png'}}" class="img-fluid" alt="Mahasankara Mini Hall Coimbatore"/></a>
        </div>
      </div>
   </div>
 


  <nav class="navbar navbar-expand-lg navbar-light bg-orange">
    <div class="container">      
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav">
              <li class="nav-item active">
                  <a class="nav-link" href="{{url('/')}}">Home</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="{{url('/about-us')}}">About Us</a><!--about-us.html-->
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="{{url('/events')}}">Events</a><!--events.html-->
              </li>
               <li class="nav-item">
                  <a class="nav-link" href="{{url('/tariff')}}">Tariff</a><!--gallery.html-->
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="{{url('/gallery')}}">Gallery</a> <!--gallery.html-->
              </li>
            </ul>

          <ul class="navbar-nav">
              <li class="nav-item">

                  <a class="nav-link" href="{{url('/contact')}}">Get in touch</a>
              </li>
          </ul>
          
      </div>
    </div>
  </nav>
</section>
   



