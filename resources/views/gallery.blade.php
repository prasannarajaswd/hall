@include('front_header')
    
     
 <section class="section gallery-sec">
  <div class="container">
    <div class="row">
      <div class="gallery grid">
        <h3>Hall Images</h3> 
          <ul class="gallery">
           
            <li><img src="images/gallery/h1.jpg" style="width:100%" onclick="openModal();currentSlide(1)" class="hover-shadow cursor"></li>
            <li><img src="images/gallery/h2.jpg" style="width:100%" onclick="openModal();currentSlide(2)" class="hover-shadow cursor"></li>
            <li><img src="images/gallery/h3.jpg" style="width:100%" onclick="openModal();currentSlide(3)" class="hover-shadow cursor"></li>
            <li><img src="images/gallery/h4.jpg" style="width:100%" onclick="openModal();currentSlide(4)" class="hover-shadow cursor"></li>
            <li><img src="images/gallery/h5.jpg" style="width:100%" onclick="openModal();currentSlide(5)" class="hover-shadow cursor"></li>
            <li><img src="images/gallery/h6.jpg" style="width:100%" onclick="openModal();currentSlide(6)" class="hover-shadow cursor"></li>
            <li><img src="images/gallery/h7.jpg" style="width:100%" onclick="openModal();currentSlide(7)" class="hover-shadow cursor"></li>
            <li><img src="images/gallery/h8.jpg" style="width:100%" onclick="openModal();currentSlide(8)" class="hover-shadow cursor"></li>
          </ul>
      </div>  
      <div class="gallery grid">
        <h3>Room Images</h3> 
          <ul class="gallery">
           
            <li><img src="images/gallery/r1.jpg" style="width:100%" onclick="openModal1();currentSlide1(1)" class="hover-shadow cursor"></li>
            <li><img src="images/gallery/r2.jpg" style="width:100%" onclick="openModal1();currentSlide1(2)" class="hover-shadow cursor"></li>
            <li><img src="images/gallery/r3.jpg" style="width:100%" onclick="openModal1();currentSlide1(3)" class="hover-shadow cursor"></li>
            <li><img src="images/gallery/r4.jpg" style="width:100%" onclick="openModal1();currentSlide1(4)" class="hover-shadow cursor"></li>
            <li><img src="images/gallery/r5.jpg" style="width:100%" onclick="openModal1();currentSlide(5)" class="hover-shadow cursor"></li>
            <li><img src="images/gallery/r6.jpg" style="width:100%" onclick="openModal1();currentSlide1(6)" class="hover-shadow cursor"></li>
          </ul>
      </div>                   
    </div>
  </div>
</section>

<!-- modal slider -->
<div id="myModal" class="modal gallery-sec">
  <span class="close cursor" onclick="closeModal()">&times;</span>
  <div class="modal-content">

    <div class="mySlides">
      <div class="numbertext">1 / 8</div>
      <img src="images/gallery/h1.jpg" style="width:100%">
    </div>

    <div class="mySlides">
      <div class="numbertext">2 / 8</div>
      <img src="images/gallery/h22.jpg" style="width:100%">
    </div>

    <div class="mySlides">
      <div class="numbertext">3 / 8</div>
      <img src="images/gallery/h3.jpg" style="width:100%">
    </div>
    
    <div class="mySlides">
      <div class="numbertext">4 / 8</div>
      <img src="images/gallery/h4.jpg" style="width:100%">
    </div>

    <div class="mySlides">
      <div class="numbertext">5 / 8</div>
      <img src="images/gallery/h5.jpg" style="width:100%">
    </div>

    <div class="mySlides">
      <div class="numbertext">6 / 8</div>
      <img src="images/gallery/h6.jpg" style="width:100%">
    </div>

    <div class="mySlides">
      <div class="numbertext">7 / 8</div>
      <img src="images/gallery/h7.jpg" style="width:100%">
    </div>

    <div class="mySlides">
      <div class="numbertext">8 / 8</div>
      <img src="images/gallery/h8.jpg" style="width:100%">
    </div>
    
    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
    <a class="next" onclick="plusSlides(1)">&#10095;</a>

  </div>
</div><!-- /. modal slider -->

    
    <!-- modal slider -->
<div id="myModal1" class="modal gallery-sec">
  <span class="close cursor" onclick="closeModal1()">&times;</span>
  <div class="modal-content">

    <div class="mySlides1">
      <div class="numbertext">1 / 6</div>
      <img src="images/gallery/r1.jpg" style="width:100%">
    </div>

    <div class="mySlides1">
      <div class="numbertext">2 / 6</div>
      <img src="images/gallery/r2.jpg" style="width:100%">
    </div>

    <div class="mySlides1">
      <div class="numbertext">3 / 6</div>
      <img src="images/gallery/r3.jpg" style="width:100%">
    </div>
    
    <div class="mySlides1">
      <div class="numbertext">4 / 6</div>
      <img src="images/gallery/r4.jpg" style="width:100%">
    </div>

    <div class="mySlides1">
      <div class="numbertext">5 / 6</div>
      <img src="images/gallery/r5.jpg" style="width:100%">
    </div>

    <div class="mySlides1">
      <div class="numbertext">6 / 6</div>
      <img src="images/gallery/r6.jpg" style="width:100%">
    </div>

    
    <a class="prev" onclick="plusSlides1(-1)">&#10094;</a>
    <a class="next" onclick="plusSlides1(1)">&#10095;</a>

  </div>
</div><!-- /. modal slider -->
    
@include('front_footer')




