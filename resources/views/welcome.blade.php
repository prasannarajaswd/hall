<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="{{'front_assets/css/bootstrap.min.css'}}" rel="stylesheet" type="text/css" />
    <!-- Style CSS -->
    <link href="{{'front_assets/css/style.css'}}" rel="stylesheet" type="text/css" />
     <!-- Responsive CSS -->
    <link href="{{'front_assets/css/responsive.css'}}" rel="stylesheet" type="text/css" />
     <link rel="shortcut icon" type="image/png" href="{{'front_assets/images/fav.png'}}"/>
    <title>Mahasankara Min Hall Vadavalli - Mini Hall at Vadavalli, Ritual Halls in Vadavalli</title>
    <meta name="description" content="Mahasankara is one of the best and affordable AC mini halls in Coimbatore, Book our mini hall for all your auspicious celebrations to experience divine bliss."/>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-140595840-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-140595840-1');
    </script>

    <script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "LocalBusiness",
      "name": "Mahasankara Mini Hall",
      "image": "https://mahasankara.com/images/logo_new.png",
      "@id": "",
      "url": "https://mahasankara.com",
      "telephone": "98410 30770",
      "address": {
        "@type": "PostalAddress",
        "streetAddress": "Plot No 5, Door No. 1/29-A 1 KS Gardens (Near Ananya's Nana Nani - 4th Phase Kasthurinaickenpalayam, Vadavalli",
        "addressLocality": "Coimbatore",
        "postalCode": "641041",
        "addressCountry": "IN"
      },
      "geo": {
        "@type": "GeoCoordinates",
        "latitude": 11.0399249,
        "longitude": 76.90895990000001
      },
      "openingHoursSpecification": {
        "@type": "OpeningHoursSpecification",
        "dayOfWeek": [
          "Monday",
          "Tuesday",
          "Wednesday",
          "Thursday",
          "Friday",
          "Saturday"
        ],
        "opens": "10:00",
        "closes": "18:00"
      },
      "sameAs": [
        "https://twitter.com/mahasankara",
        "https://www.linkedin.com/company/mahasankara-mini-hall/",
        "https://www.facebook.com/Mahasankaraminihall/"
      ]
    }
    </script>
  </head>
  <body>
  
  <!-- PAGE LOADER -->
        <div id="pageloader">
          <!-- <div class="lds-hourglass"></div> -->
          <div>
            <img src="{{'front_assets/images/icon/om.png'}}" class="img-fluid" alt=""/>
            <p>HARA HARA SANKARA JAYA JAYA SANKARA!!</p>
          </div>
        </div>
  
 <div class="page-wrapper">

   <section class="section no-padding home-section">
     <div class="container">
        <div class="row">
          <div class="col">
            <img src="{{'front_assets/images/icon/1.png'}}" class="img-fluid" alt="Mahasankara Mini Hall Coimbatore"/>
            <h1>"Life sans love towards fellow beings is incomplete. Do good to others through social service and utilize your lives to contribute to the society"</h1>
            <div class="left">
              <h3>make every event in your life most memorable and filled with divinity</h3>
            </div>
            <div class="center">
              <img src="{{'front_assets/images/home_new.png'}}" class="img-fluid" alt="Mahasankara Mini Hall Coimbatore"/>
              <div class="blink"><a href="{{url('/next')}}">click to enter</a></div>
            </div>
            <div class="right">
              <h3>make every event in your life most memorable and filled with divinity</h3>
            </div>
          </div>
            <!-- cloud -->
          <div class="cloud">   
          </div><!-- /.cloud -->
        </div>

     </div>
          
   </section>

   <ul class="social sr-only">
    <li><a href="https://www.facebook.com/Mahasankaraminihall/" target="_blank"><img src="{{'front_assets/images/icon/facebook.png'}}" class="img-fluid" alt=""/></a></li>
    <li><a href="https://twitter.com/mahasankara" target="_blank"><img src="{{'front_assets/images/icon/twitter.png'}}" class="img-fluid" alt=""/></a></li>
    <li><a href="https://www.linkedin.com/company/mahasankara-mini-hall" target="_blank"><img src="{{'front_assets/images/icon/linkedin.png'}}" class="img-fluid" alt=""/></a></li>
  </ul>

   </div>

 


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script type="text/javascript" src="{{'front_assets/js/jquery-3.3.1.slim.min.js'}}"></script>
    <script type="text/javascript" src="{{'front_assets/js/popper.js'}}"></script>
    <script type="text/javascript" src="{{'front_assets/js/bootstrap.min.js'}}"></script>

     <script type="text/javascript" src="{{'front_assets/js/jquery.min.js'}}"></script>

    
    <script type="text/javascript" src="{{'front_assets/js/custom.js'}}"></script>

    
  </body>
</html>




