@include('front_header')
    
    <section class="section">
      <div class="container">
        <!-- ROW -->
        <div class="row">
         
           <!-- COLUMN -->
          <div class="col-md-12">
            <div class="grid align-center">
              <h3>About Mini Hall</h3>
              <p>It was a divine enlightenment on the promoter that led to the building of this MAHA SANKARA HALL which is now dedicated to conducting various rituals as per the Sanatana Vaideeka Marga, to the extent possible. The Hall is built in a peaceful and serene location, in order to make it convenient for the rituals to happen in a divine way, without the disturbances of the modern world.</p>
              <p>In order to journey through the Indian spiritual hinterland and understand the meaning of spirituality in its deepest dimensions we provide a conducive venue for:</p>
              <p class="spl-pp">“Shastiapthapoorthi, Sadhabhishkam, Bheemaratha Shanthi, Kanakabhishekam, Ayush Homam, Upanayanam, Seemandham, Satsang and Spiritual Discourses”</p>
              <h3 style="margin-bottom: 20px;">Hall Facilities</h3>
              <div class="star-grid">

                <img src="images/icon/star.png" class="img-fluid" alt="Mahasankara Mini Hall Coimbatore"/>
                <p>Enjoy the peaceful ambience of nature's lap</p>
              </div>
              <div class="star-grid">
                <img src="images/icon/star.png" class="img-fluid" alt="Mahasankara Mini Hall Coimbatore"/>
                <p>Hall Capacity:<br/> 200 + seating,</br> 70 diners at a time</p>
              </div>
              <div class="star-grid">
                <img src="images/icon/star.png" class="img-fluid" alt="Mahasankara Mini Hall Coimbatore"/>
                <p>In-built Suite room & Guest room</br> with A/C</p>
              </div>

              <div class="star-grid">
                <img src="images/icon/star.png" class="img-fluid" alt="Mahasankara Mini Hall Coimbatore"/>
                <p>In-house kitchen for heating and frying</p>
              </div>
              <div class="star-grid">
                <img src="images/icon/star.png" class="img-fluid" alt="Mahasankara Mini Hall Coimbatore"/>
                <p>24 hour uninterrupted power supply and valet parking</p>
              </div>
             

              
            </div>
          </div><!-- /. COLUMN -->
           
        </div><!-- /.ROW -->
      </div>
    </section>
    
@include('front_footer')




