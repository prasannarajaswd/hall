@include('front_header')
    
      <section class="section">
      <div class="container">
        <!-- ROW -->
        <div class="row">
         
           <!-- COLUMN -->
          <div class="col-md-12">
            <div class="grid">
              <h3 class="font-1 letter-spacing-2">paropakaram Idam shareeram</h3>
              <h4 class="sub-content">Services Rendered By Us</h4>
              <p >Organizing an event for an occasion exact time, money man power and resources. Mahasankara is the boon for such physically and emotionally stressed times. We orient our best resources and provide a very peaceful experience.  We not only facilitate the banquet hall space but also help with managing the events with proficiency.</p>
              <p>Mahasankara Team can hand hold and support you right from:</p>
              <p class="spl-pp">Event Management | Matrimonial Services | Local tours & Pilgrimage Trips</br>
Function Design | Catering | Video & Photography | Rooms for Stay and Etc
</p>
              <p class="spl-p">We also promote social values by extending our space and resource for Music & Classical Dance Programs, Engagements and Family Reunions</p>
              
             
             
             <p>Every event will be diligently handled by our well experienced and trained staff. We believe that any event or ritual is made memorable with the quality of food served to the invitees. We have appointed reliable and professional caterers to meet your cuisine requirements.</p>
             <p>The ambience of the hall which adds to the consummate success of a function shall be set according to your choice that suits your budget and the occasion.</p>
             <p>In short you will have the satisfaction of paying individual attention to the needs of your kith and kin.</p>
            </div>
          </div><!-- /. COLUMN -->
           
        </div><!-- /.ROW -->
      </div>
    </section>
    
@include('front_footer')




