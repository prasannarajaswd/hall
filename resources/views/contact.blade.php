@include('front_header')
    
 <section class="section grid">
      <div class="container">
        <!-- ROW -->
        <div class="row">
         
           <!-- COLUMN -->
          <div class="col-md-6">
            <div class="contact grid">
              <h3 class="align-left">Get in touch</h3>

              
               <img src="images/icon/location.png"/>
               <address>
                <h5 class="align-left">Mahasankara Mini Hall</h5>Plot No 5, Door No. 1/29-A 1,</br>
                KS Gardens (Near Ananya's Nana Nani - 4th Phase),</br>
                Kasthurinaickenpalayam, Vadavalli,</br>
                Coimbatore - 641 041</br>
                </address>
                <img src="images/icon/phone.png"/>
                <p><a href="tel:+91 98410 30770" class="display-inline">+91 98410 30770</a>,<a href="tel:+91 96557 30770" class="display-inline">+91 96557 30770</a></p>
                <img src="images/icon/mail.png"/>
                <a href="mailto:contact@mahasankara.com">contact@mahasankara.com</a>
              
              <h3 class="align-left">Enquiry</h3>

        <form name="form" method="POST" action="contact.php">
          <input type="text" id="name" class="form-control" name="name" placeholder="Name" value='' required>
          <input type="number" id="mobile" class="form-control" name="phone" placeholder="Mobile" value='' pattern="[0-9]{10}" required>
          <input type="text" id="email" class="form-control" name="email" placeholder="Email" value='' required pattern="[^ @]*@[^ @]*">
          <input type="text" id="city" class="form-control" name="city" placeholder="City" value='' required>
          <textarea id="Message" class="form-control" name="message" placeholder="Message" required></textarea>
          <input type="submit" name="submit" class="btn submit" id="submit">
      </form> 
            </div>
          </div><!-- /. COLUMN -->
           <!-- COLUMN -->
          <div class="col-md-6">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3915.8264484713345!2d76.92690851433868!3d11.0516352570766!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba859d827d782ff%3A0x25b774cb4b81d82e!2sMahasankara+Mini+Hall!5e0!3m2!1sen!2sin!4v1557303540912!5m2!1sen!2sin" height="450" frameborder="0" style="border:0;width: 100%;" allowfullscreen></iframe>
          </div> 
                    
        </div><!-- /.ROW -->
       
      </div>
    </section>

    
@include('front_footer')




