

      <div class="footer">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6">
            <p>Copyrights © Mahasankara</p>
          </div>
          <div class="col-md-6 col-sm-6">
            <p class="footer_link">Design and Developed by <a href="https://dropout.company/" target="_blank">Dropout Creative Studio LLP</a></p>
          </div>
        </div>
     </div>
   </div>
    
    <ul class="social">
    <li><a href="https://www.facebook.com/Mahasankaraminihall/" target="_blank"><img src="{{'front_assets/images/icon/facebook.png'}}" class="img-fluid" alt=""/></a></li>
    <li><a href="https://twitter.com/mahasankara" target="_blank"><img src="{{'front_assets/images/icon/twitter.png'}}" class="img-fluid" alt=""/></a></li>
    <li><a href="https://www.linkedin.com/company/mahasankara-mini-hall" target="_blank"><img src="{{'front_assets/images/icon/linkedin.png'}}" class="img-fluid" alt=""/></a></li>
  </ul>
  

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script type="text/javascript" src="{{'front_assets/js/jquery-3.3.1.slim.min.js'}}"></script>
    <script type="text/javascript" src="{{'front_assets/js/popper.js'}}"></script>
    <script type="text/javascript" src="{{'front_assets/js/bootstrap.min.js'}}"></script>

     <script type="text/javascript" src="{{'front_assets/js/custom.js'}}"></script>

  
  </body>
</html>




