<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bookings';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['booking_id', 'from_date', 'to_date', 'package', 'timing', 'function_id', 'function_id_value', 'number_of_people', 'catering', 'photography', 'video', 'event_management', 'travel_arrangement', 'rooms', 'g_name', 'g_email', 'g_phone', 'g_address', 'g_city', 'g_state', 'g_pincode'];

    
}
