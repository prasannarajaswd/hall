<?php

namespace App\Http\Controllers\Bookings;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Booking;
use Illuminate\Http\Request;

class BookingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $bookings = Booking::where('booking_id', 'LIKE', "%$keyword%")
                ->orWhere('from_date', 'LIKE', "%$keyword%")
                ->orWhere('to_date', 'LIKE', "%$keyword%")
                ->orWhere('package', 'LIKE', "%$keyword%")
                ->orWhere('timing', 'LIKE', "%$keyword%")
                ->orWhere('function_id', 'LIKE', "%$keyword%")
                ->orWhere('function_id_value', 'LIKE', "%$keyword%")
                ->orWhere('number_of_people', 'LIKE', "%$keyword%")
                ->orWhere('catering', 'LIKE', "%$keyword%")
                ->orWhere('photography', 'LIKE', "%$keyword%")
                ->orWhere('video', 'LIKE', "%$keyword%")
                ->orWhere('event_management', 'LIKE', "%$keyword%")
                ->orWhere('travel_arrangement', 'LIKE', "%$keyword%")
                ->orWhere('rooms', 'LIKE', "%$keyword%")
                ->orWhere('g_name', 'LIKE', "%$keyword%")
                ->orWhere('g_email', 'LIKE', "%$keyword%")
                ->orWhere('g_phone', 'LIKE', "%$keyword%")
                ->orWhere('g_address', 'LIKE', "%$keyword%")
                ->orWhere('g_city', 'LIKE', "%$keyword%")
                ->orWhere('g_state', 'LIKE', "%$keyword%")
                ->orWhere('g_pincode', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $bookings = Booking::latest()->paginate($perPage);
        }

        return view('bookings.index', compact('bookings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('bookings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Booking::create($requestData);

        return redirect('bookings')->with('flash_message', 'Booking added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $booking = Booking::findOrFail($id);

        return view('bookings.show', compact('booking'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $booking = Booking::findOrFail($id);

        return view('bookings.edit', compact('booking'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $booking = Booking::findOrFail($id);
        $booking->update($requestData);

        return redirect('bookings')->with('flash_message', 'Booking updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Booking::destroy($id);

        return redirect('bookings')->with('flash_message', 'Booking deleted!');
    }
}
