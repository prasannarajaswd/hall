<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use DB;

class CheckRole
{
    /** Usr.php
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()){

            $role_id_array = array(1, 2, 3);

            //auth()->user()->role_id == 1 

            if(in_array(auth()->user()->role_id, $role_id_array) ){
                return $next($request);
            }         
        }
        
        
        return redirect('/')->with('error','You have not admin access');
    }
}
