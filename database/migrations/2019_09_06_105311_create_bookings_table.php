<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('booking_id')->nullable();
            $table->date('from_date')->nullable();
            $table->date('to_date')->nullable();
            $table->string('package')->nullable();
            $table->time('timing')->nullable();
            $table->integer('function_id')->nullable();
            $table->integer('function_id_value')->nullable();
            $table->string('number_of_people')->nullable();
            $table->integer('catering')->nullable();
            $table->integer('photography')->nullable();
            $table->integer('video')->nullable();
            $table->integer('event_management')->nullable();
            $table->integer('travel_arrangement')->nullable();
            $table->integer('rooms')->nullable();
            $table->string('g_name')->nullable();
            $table->string('g_email')->nullable();
            $table->string('g_phone')->nullable();
            $table->text('g_address')->nullable();
            $table->string('g_city')->nullable();
            $table->string('g_state')->nullable();
            $table->string('g_pincode')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bookings');
    }
}
